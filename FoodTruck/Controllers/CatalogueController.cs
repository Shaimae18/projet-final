﻿using FoodTruck.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FoodTruck.Controllers
{
    public class CatalogueController : Controller
    {
        // GET: Catalogue
        foodtruckEntities db = new foodtruckEntities();
        public ActionResult Index(string id_Repas = "", string id_FamilleRepas = "", string searchBox = "")
        {
            List<Produit> produits = db.Produit.ToList();
            if (!string.IsNullOrEmpty(id_Repas))
                produits = produits.Where(p => p.id_FamilleRepas == Convert.ToInt32(id_FamilleRepas) && p.FamilleRepas.Repas.Where(r => r.id_Repas == Convert.ToInt32(id_Repas)).ToList().Count != 0).ToList().OrderBy(p => p.libellecommercial).ToList();
            else
                produits = produits.OrderBy(p => p.libellecommercial).ToList();
            if (!string.IsNullOrEmpty(searchBox))
                produits = produits.Where(p => p.libellecommercial.Contains(searchBox)).ToList();
           ViewBag.Repas = new SelectList(db.Repas.ToList(), "id_Repas", "libelle");
            if (!string.IsNullOrEmpty(id_Repas))
            {
                int idReps = Convert.ToInt32(id_Repas);
                ViewBag.FamilleRepas = new SelectList(db.FamilleRepas.Where(f => f.Repas.Where(r => r.id_Repas == idReps).ToList().Count != 0).ToList(), "id_FamilleRepas", "libelle");

            }
            else
            {

                ViewBag.FamilleRepas = new SelectList(db.FamilleRepas.Where(f => f.Repas.ToList().Count != 0).ToList(), "id_FamilleRepas", "libelle");

            }
            if (Session["Commande"] == null)
                Session["Commande"] = new Commande();

            return View(produits);
        }
        public ActionResult Rechercher(string searchBox)
        {
            return RedirectToAction("Index", "Catalogue");
        }


        public ActionResult Add(int id)
        {

            var selectedProduit = db.Produit.Find(id);

            if (selectedProduit == null)
                return HttpNotFound();
            if (Session["produit"] == null)
                Session["produit"] = new List<Produit>();
            Ligne_Commande ligneCommande;
            var currentCommande = (Session["Commande"] as Commande);
            if (!((List<Produit>)Session["produit"]).Exists(x => x.id_Produit == selectedProduit.id_Produit))
            {

                ((List<Produit>)Session["produit"]).Add(selectedProduit);
                if (currentCommande.Ligne_Commande == null)
                    currentCommande.Ligne_Commande = new List<Ligne_Commande>();
                ligneCommande = new Ligne_Commande()
                {
                    prix_unitaire = selectedProduit.prix_unitaire,
                    quantite = 1,
                    id_Produit = selectedProduit.id_Produit

                };
                currentCommande.Ligne_Commande.Add(ligneCommande);
            }
            else
            {

                currentCommande.Ligne_Commande.Where(l => l.id_Produit == selectedProduit.id_Produit).FirstOrDefault().quantite++;
            }



            return RedirectToAction("Index", "Catalogue", new { Url.RequestContext.RouteData.Values }); ;
        }
        public ActionResult Delete(int id)
        {

            var selectedProduit = db.Produit.Find(id);

            if (selectedProduit == null)
                return HttpNotFound();
            if (Session["produit"] == null)
                Session["produit"] = new List<Produit>();
            Ligne_Commande ligneCommande;
            var currentCommande = (Session["Commande"] as Commande);
            if (((List<Produit>)Session["produit"]).Exists(x => x.id_Produit == selectedProduit.id_Produit))
            {

                ((List<Produit>)Session["produit"]).Remove(selectedProduit);
                if (currentCommande.Ligne_Commande == null)
                    currentCommande.Ligne_Commande = new List<Ligne_Commande>();
                ligneCommande = currentCommande.Ligne_Commande.Where(l => l.id_Produit == selectedProduit.id_Produit).FirstOrDefault();
                if(ligneCommande != null && ligneCommande.quantite>1 )
                    currentCommande.Ligne_Commande.Where(l => l.id_Produit == selectedProduit.id_Produit).FirstOrDefault().quantite--;
                else
                currentCommande.Ligne_Commande.Remove(ligneCommande);
            }
            



            return RedirectToAction("Index", "Catalogue", new { Url.RequestContext.RouteData.Values }); ;
        }
    }
}