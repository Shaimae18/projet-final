﻿using FoodTruck.Data;
using FoodTruck.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FoodTruck.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private foodtruckEntities db = new foodtruckEntities();
        // GET: Cart
        public ActionResult Index()
        {
            var vm = new CommandeViewModel();

            using (var context = new foodtruckEntities())
            {
                var listProduit = context.Produit.ToList();

                var dto = new AccueilData();

                foreach (Produit produit in listProduit)
                {
                    vm.listProduit.Add(produit);
                }
            }

            //create a commande for the cart
            Ligne_Commande LC = new Ligne_Commande();
            LC.id_Ligne_Commande = 0;
            LC.prix_unitaire = 10;
            LC.quantite = 2;
            LC.Repas_libelle = "oui";
            LC.id_Commande = 0;
            LC.id_Produit = 5;

            db.Ligne_Commande.Add(LC);

            Session["Commande"] = new List<Ligne_Commande>();
            ((List<Ligne_Commande>)Session["Commande"]).Add(LC);

            return View(vm);
        }

        public ActionResult Add(int id)
        {

            var selectedCommande = db.Ligne_Commande.Find(id);

            if (selectedCommande == null)
                return HttpNotFound();

            if (!((List<Ligne_Commande>)Session["Commande"]).Exists(x => x.id_Ligne_Commande == selectedCommande.id_Ligne_Commande))
            {

                var dto = new Ligne_Commande();
                dto = selectedCommande;

                ((List<Ligne_Commande>)Session["Commande"]).Add(dto);
            }
            else
            {
                var newSelectedProduit = ((List<Ligne_Commande>)Session["Commande"]).FirstOrDefault(x => x.id_Commande == selectedCommande.id_Commande);
                ((List<Ligne_Commande>)Session["Commande"]).Remove(newSelectedProduit);
            }

            return RedirectToAction("ToutesLesProduits", "Produits");
        }

        public ActionResult Delete(int id)
        {
            var selectedCommande = db.Ligne_Commande.Find(id);

            if (selectedCommande == null)
                return HttpNotFound();

            if (((List<Ligne_Commande>)Session["Commande"]).Exists(x => x.id_Commande == selectedCommande.id_Commande))
            {
                var newSelectedProduit = ((List<Ligne_Commande>)Session["Commande"]).FirstOrDefault(x => x.id_Commande == selectedCommande.id_Commande);
                ((List<Ligne_Commande>)Session["Commande"]).Remove(newSelectedProduit);
            }

            return RedirectToAction("Index");
        }

        public ActionResult ResetCart()
        {
            ((List<Ligne_Commande>)Session["Commande"]).Clear();

            return RedirectToAction("Index");
        }

    }
}
