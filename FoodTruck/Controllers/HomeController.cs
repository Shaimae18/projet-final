﻿using FoodTruck.Data;
using FoodTruck.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FoodTruck.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var vm = new AccueilViewModels();

            using (var context = new foodtruckEntities())
            {
                var listActualite = context.Actualite.ToList();
                var listCommentaire = context.commentaire.ToList();
                var listProduit = context.Produit.ToList();
                var listLigneCommande = context.Ligne_Commande.ToList();

                var dto = new AccueilData();

                //get the actualite
                dto.Actualite = listActualite;

                //get the ligne commande
                dto.CommentaireList = new List<AccueilData.CommentaireData>();

                foreach (commentaire com in listCommentaire)
                {
                    foreach (Ligne_Commande liCom in listLigneCommande)
                    {
                        if (liCom.id_Ligne_Commande == com.id_Ligne_Commande)
                        {
                            //if found, search for the produit
                            foreach (Produit prod in listProduit)
                            {
                                if (liCom.id_Produit == prod.id_Produit)
                                {
                                    //if found add to the list
                                    dto.CommentaireList.Add(new AccueilData.CommentaireData(com, liCom, prod));
                                }
                            }
                        }
                    }
                }

                //set variable with the info
                vm.accueilData = dto;

            }
            return View(vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}