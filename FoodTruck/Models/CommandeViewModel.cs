﻿using FoodTruck.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;

namespace FoodTruck.Models
{
    public class CommandeViewModel
    {
        public CommandeViewModel()
        {
            listProduit = new List<Produit>();
        }

        public List<Produit> listProduit { get; set; }
    }
}
