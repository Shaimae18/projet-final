﻿using FoodTruck.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;

namespace FoodTruck.Models
{
    public class AccueilViewModels
    {
        public AccueilViewModels()
        {
            accueilData = new AccueilData();
        }

        public AccueilData accueilData { get; set; }
    }
}
