﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Data
{
    public class AccueilData
    {
        public List<Actualite> Actualite { get; set; }
        public List<CommentaireData> CommentaireList { get; set; }

        public class CommentaireData
        {
            public commentaire currentCommentaire { get; set; }
            public Ligne_Commande currentLigneCommande { get; set; }
            public Produit currentProduit { get; set; }
            public CommentaireData(commentaire com, Ligne_Commande liCom, Produit prod)
            {
                currentCommentaire = com;
                currentLigneCommande = liCom;
                currentProduit = prod;
            }
        }
    }
}
